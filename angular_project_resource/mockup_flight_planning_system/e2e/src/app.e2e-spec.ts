import { AppPage } from './app.po';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome mockup-flight-planning-system', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to mockup-flight-planning-system!');
  });
});
