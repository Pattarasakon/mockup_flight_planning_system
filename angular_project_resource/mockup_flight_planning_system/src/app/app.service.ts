import { Injectable, EventEmitter } from '@angular/core';
import { Mission } from './model/mission.model'
import { MeasurementSystem } from './model/measurement-system.model'
import { Unit } from './model/unit.model'
@Injectable({
  providedIn: 'root'
})
export class AppService {


public selectedMeasurementSystem = 0;
public selectedDistanceUnit = 0;
public selectedAreaUnit = 0;
public selectedGroupAreaUnit = 0;
public selectedChargingRateAreaUnit = 0;
public selectedReportAreaUnit = 0;

public chargingRateAmount = 100;

public measurementSystemlist = new Array<MeasurementSystem>();

public mission : Mission = null;

public resetSettingSelected(){

  this.selectedDistanceUnit = 0;
  this.selectedAreaUnit = 0;
  this.selectedGroupAreaUnit = 0;
  this.selectedChargingRateAreaUnit = 0;
  this.selectedReportAreaUnit = 0;
  this.chargingRateAmount = 100;
}

public createMission(missionName: string,time:string,drone:string,distance:number,altitude:number,area:number,groupArea:number) {
  this.mission = new Mission(missionName, time, drone, distance, altitude, area, groupArea);
}

public initMeasurementSystem(measurementSystemName :string, distanceUnits :Unit[], areaUnits: Unit[]) {
  this.measurementSystemlist.push(new MeasurementSystem(measurementSystemName,distanceUnits,areaUnits));
}

public addNewMeasurementSystem( measurementSystemName :string){
  this.measurementSystemlist.push(new MeasurementSystem(measurementSystemName));
}

public addNewDistanceUnit( unitName :Unit){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits){
    this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits.push(unitName);
  }
  else{
    this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits = new Array<Unit>(unitName);
  }
  
}

public addNewAreaUnit( unitName :Unit){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits){
    this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits.push(unitName);
  }
  else{
    this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits = new Array<Unit>(unitName);
  }
}

public getDistanceUnitNameBySelected(selectedDistanceUnit:number){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits){
    return this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits[selectedDistanceUnit].unitName;
  }
 return;
}

public getAreaUnitNameBySelected(selectedAreaUnit:number){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits){
    return this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits[selectedAreaUnit].unitName;
  }
  return;
}

public convertDistanceUnitValueBySelected(selectedDistanceUnit:number){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits){
    return this.mission.distanceMetreValue/this.measurementSystemlist[this.selectedMeasurementSystem].distanceUnits[selectedDistanceUnit].metreValue;
  }
  return;
}

public convertAreaUnitValueBySelected(selectedAreaUnit:number){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits){
    return this.mission.areaMetreValue/this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits[selectedAreaUnit].metreValue;
  }
  return;
}

public convertGroupAreaUnitValueBySelected(selectedGroupAreaUnit:number){
  if(this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits){
    return this.mission.groupAreaMetreValue/this.measurementSystemlist[this.selectedMeasurementSystem].areaUnits[selectedGroupAreaUnit].metreValue;
  }
  return;
}

}
