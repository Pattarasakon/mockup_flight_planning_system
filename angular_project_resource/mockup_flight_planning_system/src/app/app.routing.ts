import {Routes, RouterModule} from '@angular/router';
import { FlightPlanComponent } from './flight-plan/flight-plan.component';
import { BillingComponent } from './billing/billing.component';
import { SettingComponent } from './setting/setting.component';
import { HelpComponent } from './help/help.component';

const APP_ROUTES: Routes = [
  { path: 'flight-plan', component: FlightPlanComponent },
  { path: 'billing', component: BillingComponent },
  { path: 'setting', component: SettingComponent },
  { path: 'help', component: HelpComponent },
  { path: '', redirectTo: '/flight-plan' , pathMatch: 'full' }
];
export const routing = RouterModule.forRoot(APP_ROUTES);
