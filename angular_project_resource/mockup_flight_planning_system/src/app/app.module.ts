import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HeaderComponent } from './header/header.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { routing } from './app.routing';
import { FlightPlanComponent } from './flight-plan/flight-plan.component';
import { BillingComponent } from './billing/billing.component';
import { SettingComponent } from './setting/setting.component';
import { HelpComponent } from './help/help.component';
import { AppService } from './app.service';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FlightPlanComponent,
    BillingComponent,
    SettingComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule, ReactiveFormsModule,
    routing
  ],
  providers: [ AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
