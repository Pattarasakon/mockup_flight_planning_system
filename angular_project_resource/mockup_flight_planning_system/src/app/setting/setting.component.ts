import { Component, OnInit } from '@angular/core';
import { NgModel, FormControl } from '@angular/forms';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { AppService } from '../app.service'
import { Unit } from '../model/unit.model';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.css']
})
export class SettingComponent implements OnInit {

  mission;
  closeResult: string;

  constructor(private modalService: NgbModal, public appService: AppService) { }

  ngOnInit( ) {
  }

  addUnit(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-add-unit'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log('Closed',result);
    }, (reason) => {
      console.log('Dismissed',reason);
    });
  }

  addMeasurmentSystem(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-add-measurement-system'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log('Closed',result);
    }, (reason) => {
      console.log('Dismissed',reason);
    });
  }

  onAddDistanceUnitSubmit(form: NgForm) { 
    console.log('form.value.newUnitName',form.value.newUnitName);
    console.log('form.value.newUnitMetreValue',form.value.newUnitMetreValue);
    if (!form.value.newUnitName || !form.value.newUnitMetreValue ) {
      return;
    }

    let unit = new Unit(form.value.newUnitName,form.value.newUnitMetreValue);
    this.appService.addNewDistanceUnit(unit);
    form.resetForm();
  }

  onAddAreaUnitSubmit(form: NgForm) { 
    console.log('form.value.newUnitName',form.value.newUnitName);
    console.log('form.value.newUnitMetreValue',form.value.newUnitMetreValue);
    if (!form.value.newUnitName || !form.value.newUnitMetreValue ) {
      return;
    }

    let unit = new Unit(form.value.newUnitName,form.value.newUnitMetreValue);
    this.appService.addNewAreaUnit(unit);
    form.resetForm();
  }

  onAddMeasurmentSystemSubmit(form: NgForm) { 
    console.log('form.value.measurementSystemName',form.value.measurementSystemName);
    if (!form.value.measurementSystemName ) {
      return;
    }
   
    this.appService.addNewMeasurementSystem(form.value.measurementSystemName);
    console.log('measurementSystemlist',this.appService.measurementSystemlist);
    console.log('selectedMeasurementSystem',this.appService.selectedMeasurementSystem);
    form.resetForm();
  }

  getDistanceUnits(){
    if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].distanceUnits)
    {
      return this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].distanceUnits;
    }
    return null;
  }

  getAreaUnit(){
    if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits)
    {
      return this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits;
    }
    return null;
  }




}
