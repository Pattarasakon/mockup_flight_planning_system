import { Component, OnInit } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgForm } from '@angular/forms';
import { AppService } from './app.service';
import { Unit } from './model/unit.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'mockup-flight-planning-system';

  closeResult: string;
  subscription;
  AreaUnits = Array<Unit>(); 

  constructor(private modalService: NgbModal, public appService: AppService) { }

  createMissionModal(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-create-mission'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
      console.log('Closed',result);
    }, (reason) => {
      console.log('Dismissed',reason);
    });
  }

  ngOnInit() {
    let unit;
    let distanceUnits;
    let areaUnits;

    unit = new Unit('m',1);
    distanceUnits = new Array<Unit>();
    distanceUnits.push(unit);
    unit = new Unit('sq.m',1);
    areaUnits = new Array<Unit>(); 
    areaUnits.push(unit);

    this.appService.initMeasurementSystem('si',distanceUnits,areaUnits);
  }

  onSubmit(form: NgForm) { 
    console.log('form.value.time',form.value.time);
    console.log('form.value.drone',form.value.drone);
    console.log('form.value.distance',form.value.distance);
    console.log('form.value.altitude',form.value.altitude);
    console.log('form.value.area',form.value.area);
    console.log('form.value.groupArea',form.value.groupArea);
    
    console.log('onSubmit',form);
    if (!form.value.time || !form.value.drone || !form.value.distance || !form.value.altitude || !form.value.area || !form.value.groupArea ) {
      alert('Have some values invalid or empty.');
      return;
    }
   
    this.appService.createMission(form.value.missionName,form.value.time, form.value.drone, form.value.distance, 
                                form.value.altitude, form.value.area,form.value.groupArea);

    console.log('this.appService.mission',this.appService.mission);
   
    form.resetForm();
  }

  // convertDistanceUnitValueBySelected(){
  //   if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].distanceUnits){
  //     return this.appService.mission.distanceMetreValue/this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].distanceUnits[this.appService.selectedDistanceUnit].metreValue;
  //   }
  //   return;
  // }
  // convertDistanceUnitNameBySelected(){
  //   if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].distanceUnits){
  //     return this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].distanceUnits[this.appService.selectedDistanceUnit].unitName;
  //   }
  //  return;
  // }

  // convertAreaUnitValueBySelected(){
  //   if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits){
  //     return this.appService.mission.areaMetreValue/this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits[this.appService.selectedAreaUnit].metreValue;
  //   }
  //   return;
  // }
  // convertAreaUnitNameBySelected(){
  //   if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits){
  //     return this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits[this.appService.selectedAreaUnit].unitName;
  //   }
  //   return;
  // }

  // convertGroupAreaUnitValueBySelected(){
  //   if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits){
  //     return this.appService.mission.groupAreaMetreValue/this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits[this.appService.selectedGroupAreaUnit].metreValue;
  //   }
  //   return;
  // }
  // convertGroupAreaUnitNameBySelected(){
  //   if(this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits){
  //     return this.appService.measurementSystemlist[this.appService.selectedMeasurementSystem].areaUnits[this.appService.selectedGroupAreaUnit].unitName;
  //   }
  //   return;
  // }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
