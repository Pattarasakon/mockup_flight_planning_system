export class Mission {
    missionName: string;
    time:string;
    drone:string;
    distance:number;
    altitude:number;
    area:number;
    groupArea:number;

    distanceMetreValue:number;
    areaMetreValue:number;
    groupAreaMetreValue:number;


    constructor(missionName: string,time:string,drone:string,distance:number,altitude:number,area:number,groupArea:number){
        this.missionName = missionName;
        this.time = time;
        this.drone = drone;
        this.distance = distance;
        this.altitude = altitude;
        this.area = area;
        this.groupArea = groupArea;
        
        this.distanceMetreValue = distance;
        this.areaMetreValue = area;
        this.groupAreaMetreValue = groupArea;
    }
};