import {Unit} from './unit.model'

export class MeasurementSystem {
    measurementSystemName :string;
    distanceUnits :Array<Unit>; 
    areaUnits :Array<Unit>; 

    constructor(measurementSystemName: string,distanceUnits?:Array<Unit>, areaUnits?:Array<Unit>){
        this.measurementSystemName = measurementSystemName;
        this.distanceUnits = distanceUnits;
        this.areaUnits = areaUnits;
    }
};