export class AddUnit {
    missionName: string;
    time:string;
    drone:string;
    distance:string;
    altitude:string;
    area:string;
    groupArea:string;

    constructor(missionName: string,time:string,drone:string,distance:string,altitude:string,area:string,groupArea:string){
        this.missionName = missionName;
        this.time = time;
        this.drone = drone;
        this.distance = distance;
        this.altitude = altitude;
        this.area = area;
        this.groupArea = groupArea;
    }
};