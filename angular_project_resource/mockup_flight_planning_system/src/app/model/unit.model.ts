export class Unit {
    unitName: string;
    metreValue: number;

    constructor(unitName: string,metreValue:number){
        this.unitName = unitName;
        this.metreValue = metreValue;
    }
};